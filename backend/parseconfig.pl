#!/usr/bin/perl
#
use Config::General;

$conf = Config::General->new($ARGV[0]);

my %config = $conf->getall;

foreach my $id ( keys %{ $config{'camera'} } ) {
  
  my %cam = %{ $config{'camera'}{$id} };
  print join("\t", $id,$cam{'name'},$cam{'jpg-url'}), "\n";
}
