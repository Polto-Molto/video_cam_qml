from rest_framework.serializers import Serializer, CharField, IntegerField, BooleanField

class Video(Serializer):
    source = CharField()
    preview = CharField()
    time = IntegerField()
    isexported = BooleanField()
    eventtimestamp = CharField()

class Camera(Serializer):
    videos = Video(many=True)
    camurl = CharField()
    name = CharField()
    id = IntegerField()
