from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from api.serializers import Camera
from api.models import MotionEntry

from subprocess import Popen, PIPE

class ListUsers(APIView):
    def get(self, request, format=None):
        """
        Return a list of all users.
        """

        configreader = Popen(['../parseconfig.pl','../camserv.conf'], stdout=PIPE)

        lines = configreader.communicate()[0].decode('UTF-8').split('\n')
        c = {}
        for l in lines:
            fields = l.split('\t')
            if len(fields) <2: continue
            c[fields[0]] = {'name':fields[1],
                    'url':fields[2]}

        cameras = []
        for cid, cam in c.items():
            camera = { 'name':cam['name'], 'camurl':cam['url'], 'id':cid }
            latestvids = MotionEntry.objects.filter(camera=cid, file_type=8).all()[:5]
            camera['videos'] = []
            for vid in latestvids: 
                preview = MotionEntry.objects.filter(camera=cid, event_time_stamp=vid.event_time_stamp, file_type=1).first()
                if preview == None:
                    previewimg = "/nav.jpg"
                else:
                    previewimg = preview.url()
                camera['videos'].append( 
                        { 'time':vid.time_stamp.timestamp(),
                            'isexported':vid.is_saved(),
                            'eventtimestamp':vid.event_time_stamp,
                            'preview':previewimg, 
                            'source':vid.url()})
            cameras.append(camera)

        serializer = Camera(cameras, many=True)
                  
        return Response(serializer.data)

class ExportVideo(APIView):
    def get(self, request, camera, video_timestamp, format=None):
        vids = MotionEntry.objects.filter(camera=camera, file_type=8, event_time_stamp=video_timestamp)
        for vid in vids:
            vid.export()

        return Response('exported')
