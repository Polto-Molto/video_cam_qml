from django.db import models
from os.path import isfile
from subprocess import call

class MotionEntry(models.Model):
  camera = models.IntegerField()
  filename = models.CharField(max_length=255,primary_key=True)
  file_type = models.IntegerField()
  time_stamp = models.DateTimeField()
  text_event = models.CharField(max_length=100)
  event_time_stamp = models.CharField(max_length=100)

  def url(self):
      return self.filename.replace('/var/www/localhost/htdocs/','http://hostname/')

  def is_saved(self):
      return isfile( self.filename.replace( '/var/www/localhost/htdocs/', '/media/DATAEXPORT/') )

  def export(self):
      ret = call(['rsync', self.filename, self.filename.replace( '/var/www/localhost/htdocs/', '/media/DATAEXPORT/')])
      if ret != 0:
          raise Exception('rsync returned: %d' % ret)

  class Meta:
      managed = False
      ordering = ['-time_stamp']
      db_table = 'security'
