// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.5
Item {
    id:container
    property alias source: autoreloadimage.source;
    property alias name: autoreloadimage.name;

    property alias smooth: displayimg.smooth;

    state:"playing"
    states: [
        State {
            name:"playing"
            when: !autoreloadimage.paused
            StateChangeScript {
                name:"restart-streaming"
                script: {
                    console.log("restarting....")
                    imageloader.loadnewimage()
                }
            }
            PropertyChanges {
                target: watchdog
                running:true

            }
        },
        State {
            name:"paused"
            when: autoreloadimage.paused
            PropertyChanges {
                target: watchdog
                running:false

            }
        }

    ]

    Rectangle {
        id: autoreloadimage
        property string source: ""
        property string name:""
        color:"black"

        property bool maximized: false
        property bool paused: cameraviewer.paused

        property int frames:0

        width:parent.width;
        height: parent.height;




        Image {
            source: parent.source
            visible: false
            id:imageloader
            asynchronous: true
            function refreshimage() {
                autoreloadimage.frames = autoreloadimage.frames+1
                displayimg.source = source

            }
            function loadnewimage() {
                source = parent.source+"?"+Math.random()
            }

            onStatusChanged:  {
                if(status == Image.Ready) {
                   refreshimage()
                }

                if(!autoreloadimage.paused && ( status == Image.Ready || status == Image.Error )) {
                   loadnewimage()
                    //console.log(autoreloadimage.visible)
                }

            }

        }
        Image {
            anchors.fill: parent
            id: displayimg
            visible: !parent.paused
            //source: null

        }
        Text {
            id: nametext
            text: name
            anchors.horizontalCenter: displayimg.horizontalCenter
            anchors.top: displayimg.top
            color:"white"
            //font.pixelSize: parent.height*0.1
            font.pointSize: 12
            font.bold: true
            style: Text.Outline
            styleColor: "black"
        }
        Text {
            id: fpstext
            text: ""
            anchors.right: displayimg.right
            anchors.top: displayimg.top
            color:"white"
            //font.pixelSize: parent.height*0.1
            font.pointSize: 10
            style: Text.Outline
            styleColor: "black"
        }

        Timer {
            interval: 5000
            id:watchdog
            running:true
            repeat: true
            onTriggered: {
                if(autoreloadimage.frames ==0) {
                    imageloader.loadnewimage()
                }
                fpstext.text = autoreloadimage.frames*1000/interval
                autoreloadimage.frames=0

            }
        }

        MouseArea { anchors.fill: parent;
            onClicked: autoreloadimage.maximized = !autoreloadimage.maximized;
        }

        states: State{
            name:"maximized"
            when: autoreloadimage.maximized
            ParentChange {
                target: autoreloadimage
                parent:view
                x:0; y:0
                width:parent.width; height: parent.height

            }
            PropertyChanges { target: autoreloadimage; z:1   }
            PropertyChanges { target: container; z:1 }

        }

        transitions: Transition {
            from: "*"
            to: "maximized"
            reversible: true
            ParentAnimation {
                target: autoreloadimage
                SequentialAnimation {
                    PropertyAction { target: container; property: "z" }
                    PropertyAnimation {
                        target: autoreloadimage
                        properties: "x,y,width,height"
                        easing.type: "InOutQuart"
                        duration:1000
                    }
                }
            }

        }
    }
}
