import QtQuick 2.0

Rectangle {
    id: btn
    property string text: ""
    color: "green"

    height: txt.height + 10
    width: txt.width + 20

    signal clicked

    Text {
        id:txt
        anchors.centerIn: parent
        text: btn.text
        color: "white"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: btn.clicked()
    }

}
