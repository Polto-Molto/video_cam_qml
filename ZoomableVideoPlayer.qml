import QtQuick 2.5

Item {
    id:zoomplayer

    property alias name:player.name
    property alias source:player.source
    property alias playing:player.playing
    property string xmlsource
    property string camera

    property bool maximized:false

    states: State{
        name:"maximized"
        when: zoomplayer.maximized
        ParentChange {
            target: player
            parent:zoomedcontainer
            x:0; y:0
            width:parent.width; height: parent.height


        }
        PropertyChanges { target: player; z:1   }
        PropertyChanges { target: zoomplayer; z:1 }
        PropertyChanges { target: historyarea; model: history; camera:zoomplayer.camera }

    }

    transitions: Transition {
        from: "*"
        to: "maximized"
        reversible: true
        ParentAnimation {
            target: player
            SequentialAnimation {
                PropertyAction { target: zoomplayer; property: "z" }
                PropertyAnimation {
                    target: player
                    properties: "x,y,width,height"
                    easing.type: "InOutQuart"
                    duration:1000
                }
            }
        }

    }

    property CamVidsXmlModel history: CamVidsXmlModel {
        source: xmlsource
        camname: name
    }

    VideoPlayer {
        id:player
        //anchors.fill: parent
        width:parent.width
        height:parent.height

        MouseArea { anchors.fill: parent;
            onClicked: {
                zoomplayer.maximized = !zoomplayer.maximized

                console.log("maximized: "+zoomplayer.maximized)
            }
        }


    }

}
