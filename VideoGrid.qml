import QtQuick 2.5
import QtMultimedia 5.5

Rectangle {
    id: gridcontainer
    property alias model: gridrepeater.model
    property string saveendpoint

    color: "#000000"


    property bool playing:true
    Item {
        id: zoomedcontainer
        z:1
        /*
        property double aspect: 4./3

        anchors.centerIn: parent

            height: Math.min(parent.height,
                              parent.width/aspect)
            width: Math.min(parent.width,
                             parent.height*aspect)

                             */
        anchors.fill: videogrid

    }
        Grid {
            id:videogrid
            rows: Math.ceil( Math.sqrt(gridrepeater.count ) )
            columns: Math.ceil( gridrepeater.count/rows )
            spacing: 2

            property double aspect: 4./3
            anchors.centerIn: parent
            height: Math.min(parent.height,
                              parent.width*videogrid.rows/aspect/videogrid.columns)
            width: Math.min(parent.width,
                             parent.height*videogrid.columns*aspect/videogrid.rows)

             Repeater {
                  id: gridrepeater

                 ZoomableVideoPlayer {
                     name: model.name
                     source: model.camurl
                     xmlsource: gridcontainer.model.source
                     camera: model.id
                     width: parent.width/parent.columns
                     height: parent.height/parent.rows
                     playing: gridcontainer.playing
                 }
             }

        }
        Item {
              id: vidbuttonarea
              anchors.right: gridcontainer.right
              anchors.top: videogrid.top
              anchors.bottom: videogrid.bottom
              anchors.left: videogrid.right
              visible: historyvideo.vidobj
              Column {
                  width: parent.width

                  Rectangle {
                      visible: historyvideo.vidobj.isexported == "False"
                      width: parent.width
                      height: width/2
                      Text {
                          anchors.centerIn: parent
                          text: "SAVE"
                      }
                      MouseArea {
                          anchors.fill: parent
                          onClicked: {
                              var oReq = new XMLHttpRequest();
                              oReq.onreadystatechange = function() {
                                  gridcontainer.model.reload()
                              }
                              oReq.open("GET", gridcontainer.saveendpoint+historyarea.camera+"/"+historyvideo.vidobj.eventtimestamp);
                              oReq.send();

                          }
                      }
                  }

              }
        }

        Flickable {
            id: historyarea
            visible: historyrepeater.model !== undefined
            property string camera: ""
            anchors.right: videogrid.left
            anchors.top: videogrid.top
            anchors.bottom: videogrid.bottom
            anchors.left: gridcontainer.left
            anchors.margins: {
                right: 4
                left: 4

            }

            contentHeight: histcolumn.height
            property alias model: historyrepeater.model

            Column {
              id: histcolumn
              width: parent.width
              spacing: 10

              Repeater {
                id: historyrepeater

                Column {
                  width: parent.width
                  spacing: 3
                  Text {
                      anchors.horizontalCenter: parent.horizontalCenter
                      property date d: new Date(model.time*1000)
                      text: d.getHours() + ":" + (d.getMinutes() <10 ? "0" : "") + d.getMinutes()
                      color: "white"
                  }
                  Text {
                      anchors.horizontalCenter: parent.horizontalCenter
                      color: "white"
                      text: "[saved]"
                      visible: model.isexported == "True"

                  }

                  //opacity: vidprev.status == Image.Ready ? 1 : 0
                  Behavior on opacity {
                      NumberAnimation {
                          duration: 500
                      }
                  }

                  Image {
                    width: parent.width
                    fillMode: Image.PreserveAspectFit
                    id: vidprev
                    source: model.preview
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
			    historyvideoactual.stop()
                            historyvideo.vidobj = model
                        }
                    }
                  }
                }
              }
            }

        }
	Rectangle {
	color: 'black'
	id: historyvideo
            anchors.fill: videogrid
	property var vidobj: null
visible: vidobj != null
           z: 4
        Video {
            anchors.fill: parent
            id: historyvideoactual
           property var vidobj: null
           fillMode: VideoOutput.PreserveAspectFit
           autoPlay: true
           autoLoad: true
           source: historyvideo.vidobj ? historyvideo.vidobj.source : ""
		Text {
			text: parent.source
			color: 'white'
		}
	
        }

           MouseArea {
               anchors.fill: parent
               onClicked: {
		historyvideoactual.stop()
		historyvideo.vidobj = null
		}
           }
		Text {
anchors.bottom: parent.bottom
anchors.horizontalCenter: parent.horizontalCenter
		color: 'white'
		text: historyvideoactual.progress
		}
}
}
