import QtQuick 2.5
import QtMultimedia 5.5

Item {
    id: player
    property string source
    property bool playing: false
    property alias name: nametext.text


    Item {
        id: videooutput
        anchors.fill: parent

        states: [
             State {
                name:'image'
                when: player.playing && player.source.match(/\.jpg$/)!=null
                PropertyChanges {
                    target: displayimg
                    visible:true

                }

            },
            State {
                name:'video'
                when: player.playing && ( player.source.match(/\.amp$/)!=null || player.source.match(/\.mp4$/)!=null || player.source.match(/\.mjpe?g$/)!=null )
                PropertyChanges {
                    target: video
                    visible:true
                    playing:true

                }
            }

        ]
        Video {
            id: video
            visible:false
            source: player.source
            anchors.fill: parent
            fillMode: VideoOutput.PreserveAspectFit
            onStopped: {
                if(player.playing) {
                    console.debug("restarting video...")
                    video.play()
                }
            }



            //source:"http://10.21.0.3/jpg/image.jpg"
            //source:"rtsp://10.21.0.3/mpeg4/media.amp"
        }

        Image {
            id: displayimg
            visible:false
            anchors.fill: parent

            property double imgtime:0
            property bool playing: false
            fillMode: Image.PreserveAspectFit

            Repeater {
                model:1
                Image {
                id:prefetcher
                visible: false
                asynchronous: true
                property double imgtime:0

                states: [
                    State {
                    name:"running"
                    when: videooutput.state == "image"
                    StateChangeScript {
                        script: prefetcher.fetchimage()
                    }
                }
                ]

                function fetchimage() {
                    var d = Number(new Date())
                    prefetcher.source = player.source+"?"+d
                    prefetcher.imgtime = d
                    //console.debug("prefetcher"+index+": "+prefetcher.source)
                    fetchtimeout.start()
                }
                Timer {
                    id: fetchtimeout
                    interval: 5000
                    onTriggered: {
                        if(player.playing) {
                            console.debug("restarting prefetcher"+index)
                            fetchimage()
                        }
                    }
                }

                onStatusChanged: {
                    fetchtimeout.stop()
                    if(status == Image.Ready) {
                        if(prefetcher.imgtime > displayimg.imgtime) {
                            displayimg.source = prefetcher.source
                        }
                        if(player.playing) {
                            fetchimage()
                        }
                    }

                    if(status == Image.Error && player.playing) {
                        fetchimage()
                    }
                }

            }

            }
        }
        Rectangle {
            id:pausescreen
            visible:  videooutput.state == ''
            color:'#000020'
            anchors.fill: parent
            Text {
                text: "-- paused --"
                color: "white"
                anchors.centerIn: parent
            }
        }

        MouseArea {
            id:clickarea
            enabled: false
            anchors.fill: parent
            onClicked: {
                player.playing = !player.playing
            }
        }

    }





    Text {
        id: nametext
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        color:"white"
        //font.pixelSize: parent.height*0.1
        font.pointSize: 12
        font.bold: true
        style: Text.Outline
        styleColor: "black"
    }



}
