import QtQuick 2.5
//import Qt 4.7
import QtQuick.Window 2.0

Window {
    visible:true
    width:640
    height:480

Item {
    anchors.fill: parent
    states:
        State {
        name:"running"
        when: vgrid.playing
        StateChangeScript {
            script: playtimeout.restart()
        }
    }
    VideoGrid {

        model: CamXmlModel {source: "http://192.168.2.9:8000/cameras" }
        saveendpoint: "http://192.168.2.9:8000/export/"
        id:vgrid
       anchors.horizontalCenter: parent.horizontalCenter
       width: parent.width
       anchors.top: parent.top
       height: parent.height - buttonbar.height

       MouseArea {
           anchors.fill: parent
           z:1
           onPressed: {
               if(vgrid.playing) mouse.accepted = false
               else  vgrid.playing = true
               playtimeout.restart()
           }
       }
       Timer {


           id:playtimeout
           //running: vgrid.playing
           interval: 600000
           onTriggered: vgrid.playing = false
       }

    }

    Rectangle {
        id:buttonbar
        height: 40
        width:parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom


        color:"black"
        }



}


}
