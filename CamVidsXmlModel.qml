import QtQuick 2.0
import QtQuick.XmlListModel 2.0

XmlListModel {
    property string camname
    query: "/root/list-item[name='"+camname+"']/videos/list-item"
    XmlRole {
        name: "time"
        query: "time/string()"
    }
    XmlRole {
        name: "preview"
        query: "preview/string()"
    }
    XmlRole {
        name: "source"
        query: "source/string()"
    }
    XmlRole {
        name: "isexported"
        query: "isexported/string()"
    }
    XmlRole {
        name: "eventtimestamp"
        query: "eventtimestamp/string()"
    }

    onStatusChanged: console.log(status, errorString(), count)

}
