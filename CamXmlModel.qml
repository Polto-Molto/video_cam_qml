import QtQuick 2.0
import QtQuick.XmlListModel 2.0
XmlListModel {
    query: "/root/list-item"
    onStatusChanged: console.log(status, errorString())
    XmlRole {
        name: "name"
        query: "name/string()"
    }
    XmlRole {
        name: "camurl"
        query: "camurl/string()"
    }
    XmlRole {
        name: "id"
        query: "id/string()"
    }
}
